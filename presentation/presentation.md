# Linter und Testframeworks 

von Adrian Stritzinger

Note:
Fokus auf JS

---

## Gliederung

1. **Linter**
  1. Grundfragen
  2. Linter in JS - Ein Überblick
  3. Einführung in ESLint
2. **Testframeworks**
  1. Grundfragen
  2. Testframeworks in JS - Ein Überblick
  3. Einführung in Jest & WebdriverIO
  4. Ausblick

Note:
  - Grundfragen - Was? Warum? ...
  - Vorausgesetzt (falls etwas unbekannt, melden)
    - Mit JS, Node und NPM vertraut sein
    - Tw. VSCode, Vue, Gitlab, HTML, ... 

---

# Linter

---

## Grundfragen

~~~

### Was ist ein Linter?

Programm zur statischen Analyse (dem „Linten“) von Code hinsichtlich programmatischer und stilistischer Fehler

~~~

### Warum Linten?

Note:
 - Genauer
  - Personen aus Bereich kompilierter Sprachen häufig Problematik nicht verstehen
  - Häufig als lästig angesehen

~~~

Bestimmte Fehler und Schwachstellen beim Editieren erkennen und tw. automatisch beheben, z. B.

<pre><code class="javascript" data-trim>
  for (var i = 0; i < 10; i--) { ... } // infinite loop because of mix-up of decrement/increment
</code></pre>
<pre><code class="javascript" data-trim>
  class P{
    get name(){
      ... // getter with no returns 
    }
  }
</code></pre>
<pre><code class="javascript" data-trim>
  (() => { 
    try {
        return 1;
    } finally {
        return 3; // return in finally leads to unexpected results
    }
  })(); // returns 3 
</code></pre>

Note:
- JS ist Skriptsprache → kein "klassischer" Compiler
- Schwachstellen: Hinweise auf Abhängkeiten vom Betriebssystem
- Frühe Hinweise auf Fehler
- Code-Beispiele
  - Endlosschleife (Vergleichsoperator oder Dekrementieren falsch)
  - Getter ohne return
  - Kontrollfluss-Statements in `try` oder `catch` werden durch solche in `finally` überschrieben

~~~

Automatisierte Durchsetzung eines einheitlichen, konsistenten Stils (→ verteilte Teams) und bestimmter Qualitätsstandards (Best practices), z. B.

<pre><code class="javascript" data-trim>
  var name = "ESLint" // always semicolon vs. only when absolutely necessary
  var website = "eslint.org";
</code></pre>
<pre><code class="javascript" data-trim>
  function foo() {
    if (x) {
        return y;
    } else { // unnecessary else-block
        return z;
    }
  }
</code></pre>
<pre><code class="javascript" data-trim>
  if ("red" === color) { ... } // yoda condition
</code></pre>


Note:
- "Einheitlich" über Teammitglieder
- "Konsistent" über Codebasis
- Verständlichkeit, höhere Produktivität
- Hinzulernen von Best practices
- Code-Beispiele
  - automatic semicolon insertion (ASI)
  - return in else-Block
  - yoda statement: “if red equals the color”

---

## Linter in JS - Ein Überblick

<img class="plain logo" src="assets/jslint-logo.gif" alt="JSLint Logo">
<img class="plain logo" src="assets/jshint-logo.png" alt="JSHint Logo">
<img class="plain logo" src="assets/eslint-logo.svg" alt="ESLint Logo"><label class="logo-label">ESLint</label>

Note:
Fokus auf den 3 bekanntesten

~~~

<img class="plain logo" src="assets/jslint-logo.gif" alt="JSLint Logo">

- Ältester JS-Linter (zumindest unter den drei ausgewählten)
- Autor: [Douglas Crockford](https://www.crockford.com/linebreaking.html)
- Setzt die „guten Anteile“ von JS durch
- Kaum bis keine Konfiguration oder Erweiterbarkeit (z. B. um eigene Regeln)
- Mängel in Dokumentation
- Zuordnung von Regeln zu Regelverletzungen tw. schwierig

~~~

<img class="plain logo" src="assets/jshint-logo.png" alt="JSHint Logo">

- Fork von JSLint
- Gute Dokumentation
- Konfiguration (u. a. von Regeln, Art und Weise der Durchsetzung) mgl. und notwendig
- Unterstützung vieler Bibliotheken (u. a. NodeJS, jQuery, Mocha) und auch in grundlegender Weise von ES6 und höher
- Zuordnung von Regeln zu Regelverletzungen tw. schwierig
- Keine Unterstützung eigener Regeln

~~~

<img class="plain logo" src="assets/eslint-logo.svg" alt="ESLint Logo"><label class="logo-label">ESLint</label>

- Aktuell beliebtester Linter für JS
- Dokumentation sehr gut
- Vollständig konfigurierbar und erweiterbar (um eigene Regeln)
- Mehr Regeln als bei anderen Lintern und 
- Ausgabe schließt gebrochene Regel(n) mit ein
- Konfiguration notwendig, aber durch Templates und Codeanalyse erleichtert
- Gut integriert mit anderen Tools

Note:
  - Beliebtheit anhand tpyischer Github-Metriken ermittelt
  - Doku ausführlich, mit vielen Bsp. und (meist) verständlich, z. B. Regeln nach Kategorien geordnet und mit Beispielen versehen
  - Langsamer als andere Linter
  - „Andere Tools“: Entwicklungsumgebungen, Vue usw.

---

## Einführung in

<img class="plain logo" src="assets/eslint-logo.svg" alt="ESLint Logo"><label class="logo-label">ESLint</label>

---

# Testframeworks

Note:
Automatisierte vs. manuelle Tests

---

## Grundfragen

Note:
  - Kurz fassen, da allgemein bekannt

~~~

### Was ist Testen?

(Automatisches) Prüfen, ob (ein Teil der) Applikation die Erwartungen erfüllt (auf bestimmte Eingaben bestimmte Ausgaben liefert)

Note:
- Testen umfasst i. w. S. auch manuelles Prüfen
- Fokus auf automatischem Testen (Testframeworks hierfür gedacht)

~~~

### Warum (automatisches) Testen?

  - Fehler leichter und früher finden
  - Wiederholbar (→ unterschiedliche Umgebungen), systematisch(er), vollständig(er)
  - [Regressionstesten](https://de.wikipedia.org/wiki/Regressionstest)
  - Nachweis, dass Spezifikation erfüllt
  - Hilfe bei konstruktiver Beschreibung vor „Coden“ ([TDD](https://de.wikipedia.org/wiki/Testgetriebene_Entwicklung))
  - hohe Code-Qualität (modular, sauber)

Note:
  - Systematisch(er) im Vgl. zu? Manuelles Testen weniger systematisch, deshalb häufig Übersehen von Fehlern
  - Regressionstesten: Feststellen, ob nach Änderung Applikation immer noch erwartungsgemäß funktioniert
  - Warum eher modular? Spaghetticode schwierig zu testen
  - Endresultat: funktionierende Software, höhere Produktivität

~~~

### Arten von Tests

Note:
  - Beantworten Frage „Was ist zu testen?“ (Grundsätzlich alles)

~~~

#### Unit-Tests

Testen isolierte Programmeinheiten, d. h. Funktionen, Module, Klassen usw., z. B.

<pre><code class="javascript" data-trim>
  const sum = (...args) => args.reduce((result, arg) => result + arg, 0);
</code></pre>

<pre><code class="javascript" data-trim>
  test('1 + 2 should be 3', () => {
    expect(sum(1, 2)).toBe(3)
  })
</code></pre>

Note:
  - Code nicht unbedingt gewohnt oder schön

~~~

#### Integrationstests

Testen Integration von Programmeinheiten miteinander, z. B. eine Funktion, die wiederum andere Funktionen aufruft, z. B.

<pre><code class="javascript" data-trim>
  const dotProduct = (v, w) => sum(...v.map((valueV, index) => valueV * w[index]))
</code></pre>

<pre><code class="javascript" data-trim>
  test(`dot product of (${[1, 2, -3]}) and (${[5, 6, 9]}) should be -10`, () => {
    expect(dotProduct([1, 2, -3], [5, 6, 9])).toBe(-10)
  })
</code></pre>

Note:
  - Funktionen nicht weggemockt
  - Abhängigkeiten werden mitgetestet

~~~

#### End-to-End-Tests

Auch **e2e-**, **funktionale** oder **UI Tests**

Testen vollständige Interaktionen mit der Software, die alle Bestandteile, d. h. Frontend, Backend, Datenbank usw. miteinschließen, z. B.

<pre><code class="javascript" data-trim>
  describe('The FaPra CSCW example app for this introduction', () => {
      it('should have the title "FaPra CSCW Example App"', () => {
          browser.url('https://localhost:8080')
          const title = browser.getTitle()
          assert.strictEqual(title, 'FaPra CSCW Example App')
      })
  })
</code></pre>

Note:
  - Klassischerweise Blackbox

~~~

### Welche Tests und wie viele davon?

Testpyramide

<img class="plain" src="./assets/test-pyramid.png" alt="Testpyramide">
<a href="https://martinfowler.com/bliki/TestPyramid.html">https://martinfowler.com/bliki/TestPyramid.html</a>

Note:
  - Viele schnelle, schnell zu schreibende /kostengünstige Tests vs. komplexere, teure, „flaky“, brüchigere usw. 
  - Low-level Tests können häufig dasselbe leisten wie high-level Tests, bloß mit zusätzlichen Vorteilen verbunden
  - Fehler schon mit einfachen Tests abfangen, Tests höherer Ebenen als 2. Verteidigungslinie (jeder Fehler auf höherer Ebene sollte möglichst schon weiter unten gefangen werden)
  - Service ist Zwischengeschaltet, Integration ohne Interaktion mit UI testen
  - Weitere Arten bzw. Klassifikationen von Tests (Akzeptanztests etc.) vernachlässigt

~~~

### Wann schreibe ich Tests?

Ein Ansatz: **Testgetriebene Entwicklung (TDD)**

<span style="color: red;">R</span><span style="color: green;">G</span>R-Prinzip
  1. <span style="color: red;">Test für neue Funktionalität schreiben, der fehlschlägt</span>
  2. <span style="color: green;">Funktionalität möglichst einfach implementieren, sodass Test durchläuft</span>
  3. Refactor: Optimieren und/oder vereinfachen

Note:
  - Wie in Entwicklung eingebunden?
  - Füher
    - Testen, wenn überhaupt, meist manuell nach Fertigstellen der Applikation
    - Folge: nicht enden wollendes Bugfixing in Produktion durch Nächte hindurch, manchmal ohne Ahnung, was den Fehler verursacht hat
  - So viele (Unit-)Tests wie Produktscode
  - Unter Umständen schwierig bis unmöglich (technisches Neuland, unerfahren in Programmierung)
  - Vorteile zu Beginn schon genannt

---

## Testframeworks in JS - Ein Überblick

~~~

### Für Unit- und Integrationstests

<img class="plain logo" src="assets/jasmine-logo.svg" alt="Jasmine Logo">
<img class="plain logo" src="assets/mocha-logo.svg" alt="Mocha Logo">
<img class="plain logo first-letter-logo" src="assets/jest-green-logo.svg" alt="Jest Logo"><label class="logo-label">est</label><br>

Note:
- Nicht nur für Unit-Tests genutzt, Basis vieler anderer Frameworks (CodeceptJS, WebdriverIO)
- Überblick über ein paar der Bekanntesten

~~~

<img class="plain logo" src="assets/jasmine-logo.svg" alt="Jasmine Logo">

- Ältestes unter den vorgestellten JS-Testframeworks
- „One-Stop-Solution“ für einfache Tests - Testrunner, Assertions, Spies und Mocks enthalten
- Schlank, keine Abhängigkeiten
- Bewährt, viele Ressourcen (Artikel, Videos usw.)
- Einfach, konsistent
- Basis anderer Frameworks (→ Jest)
- Keine Konfiguration notwendig
- Tests seriell ausgeführt

~~~

<img class="plain logo" src="assets/mocha-logo.svg" alt="Mocha Logo">

- Konfiguration und Erweiterung flexibler, weitgehender und notwendig
- Erweiterbarkeit vereinfacht
- Externe Bibliotheken für Assertions (z. B. Chai), Mocks und Spies (z. B. Sinon) usw. notwendig 
- Teststruktur, -runner und -reporter bringt Mocha mit
- Keine „single source of truth“, hoher Lernaufwand
- Keine globalen Variablen für Plugins (inkonsistent)
- Optimal für ausgefallene Testszenarien
- Tests seriell ausgeführt

Note:
  - extensions and libraries are designed only to run on top of it.
  - Set-Up einmalig
  - Externe Bibliotheken können selbst gewählt werden

~~~

<img class="plain logo first-letter-logo" src="assets/jest-green-logo.svg" alt="Jest Logo"><label class="logo-label">est</label>

- Aktuell beliebtestes JS-Testframework, dynamische Entwicklung, trotzdem reliabel
- Von Facebook entwickelt und verwaltet, unter der Haube andere Frameworks
- „One-Stop-Solution“ auch für viele komplexe Tests (auch z. B. Coverage Reporter)
- Pro Test ein Prozess → isolierte, intelligente parallele, schnelle Ausführung (→ Regressionstesten; Ursachen für Testfehlschlagen klarer)

Note:
- nach Github Metriken (users, watchers, stars etc.) & NPM Metriken (Downloads)
- viele Abhängigkeiten, sehr groß
- selbst große Module einfach zu mocken
- isoliert → Tests unabhängig voneinander, kein unangenehmen Fehler (Flake)
- schnell → große Projekte
- Only updates the files updated, so tests are running very fast in watch mode.

~~~

<img class="plain logo first-letter-logo" src="assets/jest-green-logo.svg" alt="Jest Logo"><label class="logo-label">est - Fortsetzung</label>s

- Funktioniert „out-of-the-box“, aber auch konfigurier- und erweiterbar
- Gut integriert mit anderen Frameworks (z. B. Vue) und Tools (z. B. Babel)
- Einfach, aber doch mächtig (z. B. Snapshot-Tests)
- Fokussiert in Ausnahmefällen stark auf React
- Breite, klare API

~~~

### Für End-to-End-Tests

<img class="plain logo" src="assets/webdriverio-logo.png" alt="WebdriverIO Logo">

~~~

#### Webdriver <span id="webdriverio-io-logo">I/O</span><img class="plain logo" src="assets/webdriverio-logo.png" alt="WebdriverIO Logo">

- Eigene, vollständige Implementierung des [W3C WebDriver Protokolls]((https://w3c.github.io/webdriver/))
- Automatisierung aller gängigen Browser sowie mobiler Applikationen (→[Appium](http://appium.io))
- Einfache Kommandos, bekannte Testruktur (z. B. Jasmine)
- Flexibel konfigurier- und erweiterbar (etliche Reporter und Services)
- Einfache Installation (NPM Paket) und Konfiguration (CLI Programm)

~~~

#### Webdriver <span id="webdriverio-io-logo">I/O</span><img class="plain logo" src="assets/webdriverio-logo.png" alt="WebdriverIO Logo"> - Fortsetzung

- Eigene Kommandos und Seitenobjekte können erzeugt werden, z. B. statt

```javascript
  browser
    .url("login-page")
    .setValue("#username", "testuser")
    .setValue("#password", "hunter2")
    .click("#login-btn")
```
```javascript
  LoginPage.open()
  LoginPage.login('testuser', 'hunter2')
```

Note:
  - W3C WebDriver Protokoll: Remotes Kontrollinterface zur Introspektion und Kontrolle von User Agents (Browsern). Platform- und sprachneutrales Wire Protokol. Programme außerhalb des Prozesses können von außen das Verhalten des Browsers bestimmen. Basiert auf Selenium WebDriver.
  - Selenium / WebDriver is for web applications only.

~~~

#### Weitere End-to-End-Test-Frameworks
<img class="plain logo" src="assets/testcafe-logo.svg" alt="TestCafé Logo">
<img class="plain logo" src="assets/cypress-logo.webp" alt="Cypress Logo">

Note:
  - Testcafé ist keine echte Automatisierung, Browser weiß nicht, dass er kontrolliert wird
  - Cypress unterstützt das Testen mobiler Applikationen und einiger Browser (z. B. Firefox) nicht

---

## Einführung in

<img class="plain logo first-letter-logo" src="assets/jest-green-logo.svg" alt="Jest Logo"><label class="logo-label">est</label>

## und 

<img class="plain logo" src="assets/webdriverio-logo.png" alt="WebdriverIO Logo">

---

## Ausblick

~~~

### Visuelles Regressionstesten

Ist-Zustand einer Applikation/Website wird visuell mit Soll-Zustand verglichen

<img class="plain" src="./assets/visual-regression-testing-example.png" alt="Beispiel für visuelles Regressionstesten">
<a href="https://css-tricks.com/visual-regression-testing-with-phantomcss/">https://css-tricks.com/visual-regression-testing-with-phantomcss/</a>

Note:
  - Service hierfür auch für WebdriverIO verfügbar (Applitools)

~~~

### Verhaltensgetriebene Entwicklung mit [Cucumber](https://cucumber.io/) und Co.

Alle Stakeholder ins Bot holen durch Transformation von Spezifikationen/Akzeptanzkriterien in natürlicher Sprache in ausführbare Tests

<pre><code class="gherkin" data-trim>
  # Comment
  @tag
  Feature: Is it Friday yet?
    Everybody wants to know when it's Friday

    Scenario: Sunday isn't Friday
      Given today is Sunday
      When I ask whether it's Friday yet
      Then I should be told "Nope"
</code></pre>

Note:
- DSL: Gherkin
- 1. Zeile: Bezeichnung des Features
- 2. Zeile: Kurzbeschreibung des Features

~~~

### und vieles mehr ...

---

Danke für eure Aufmerksamkeit! 

Diese Präsentation wurde mit [reveal.js](https://github.com/hakimel/reveal.js) erstellt.

Eine Auflistung verwendeter Quellen findet sich im zugehörigen Wikieintrag unter dem Punkt „Weiterführende Informationen“.

[Hier geht es zu den Codebeispielen.](https://gitlab.com/adi-wan/fapra-intro-testing-linting)


