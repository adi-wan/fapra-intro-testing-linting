# fapra-intro-testing-linting

Fachpraktikum CSCW 2019/20: Codebeispiele und Beispielprojekte zum Impulsvertrag in Präsenzphase I zu Lintern und Testframeworks

Damit die Beispielapp ausgeführt werden kann, ist ein Connection String zum Herstellen der Verbindung zur Datenbank notwendig. Diesen können Sie von mir anfordern. Alternativ können Sie auch Ihre eigene Datenbank anbinden.
