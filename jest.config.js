module.exports = {
  collectCoverage: true,
  collectCoverageFrom: [
    'tests/unit/**'
  ],
  testMatch: ['**/fapra-intro-testing-linting/tests/**/?(*.)+(spec|test).[jt]s?(x)'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest'
  }
}
