module.exports = {
  env: {
    node: true,
    jest: true
  },
  extends: [
    'standard'
  ],
  globals: {
    Atomics: 'readonly',
    browser: 'readonly',
    SharedArrayBuffer: 'readonly',
    $: 'readonly',
    $$: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  rules: {
    // "semi": ["error", "always"],
    // "yoda": 'warn',
    // "no-undef": 'warn'
    'standard/no-callback-literal': 'off'
  }
}
