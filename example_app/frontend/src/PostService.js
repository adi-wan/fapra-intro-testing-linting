import axios from 'axios'

const URL = '/api/posts/'

class PostService {
  static getPosts() {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.get(URL)
        const data = res.data
        resolve(
          data.map(post => ({
            ...post,
            createdAt: new Date(post.createdAt)
          }))
        );
      } catch (err) {
        reject(err)
      }
    })
  }

  static insertPost(text) {
    return axios.post(URL, {
      text
    })
  }
  
  static deletePost(id) {
    return axios.delete(`${URL}${id}`)
  }
}

export default PostService
