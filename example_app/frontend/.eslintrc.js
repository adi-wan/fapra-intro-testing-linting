module.exports = {
    "env": {
        "browser": true,
        "node": true,
        "jasmine": true,
        "jest": true
    },
    "extends": [
        "plugin:vue/essential"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module",
        "parser": "babel-eslint"
    },
    "plugins": [
        "vue"
    ],
    "root": true,
}
