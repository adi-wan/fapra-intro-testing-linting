# Running application in docker

Prerequisite: Docker installed and running

If you have cloned, downloaded or forked the project, you need to build the docker image first by running the following from within this directory inside of a shell:

```
docker build -t fapra_cscw_example_app:alpine .
```

After you have the image, you can start the application by executing the following inside of a shell (make sure that nothing is running on port 8080 of your localhost or change the port mapping in the following command):

```
docker run -e NODE_ENV=production -p 8080:5000 fapra_cscw_example_app:alpine
```

You should be able to open the application by opening your favourite browser and navigating to `localhost:8080`.
