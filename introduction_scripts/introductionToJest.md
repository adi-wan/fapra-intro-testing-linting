# Installation

*Rahmenprojekt erklären - nur Beispiele*

*Pakete in `package.json` zeigen*
  - `jest`
    - `babel-jest` indirekte Dependency von `jest`, transformiert Dateien automatisch, falls eine Babelkonfiguration (`.babelrc`) im Projekt existiert
  - `@types/jest` für (TypeScript) Typdefinitionen und globale Variablen für Jest (für CodeSnippets, Autovervollständigung, Doku-Popover)

# Struktur eines Tests

*Spezifikation von `filterByTerm()` erklären*
Eingabe: 
  - Array von Objekten mit Property `url`, deren Wert eine URL in Form eines Strings ist
  - Term (String)
Ausgabe:
  - Teilarray des Eingabearrays mit Objekten, deren `url` Property Term ohne Beachtung von Groß- und Kleinschreibung enthält

*Struktur, einzelne Funktionen, Spezifikation anhand von `tests/filterByTerm.spec.js` weiter erläutern*

Struktur
  1. Import des zu testendenen Moduls
  2. Definition der Eingabe
  3. Definition der Erwartung an Ausgabe
  4. Vergleich von Erwartung mit tatsächlicher Ausgabe

Einzelne Funktionen
  - `describe` für Testsuite, nimmt Beschreibung und Callback Funktion als Wrapper
  - `test` für eigentlichen Test, ähnlich wie `describe`
  - `expect` und Matcher (Funktion zum Vgl. von Erwartung und Realität) erklären; eigentlicher Test

*Set up und Tear down sowie TDD andhand von `tests/filterByTermTDD.spec.js` durchgehen (siehe unten)*

# Testausführung

*`maths.js` zeigen, um zu zeigen, was wir noch testen*

*`jest` erklären*
  - sucht nach Tests mittels regulärer Ausdrücke
  - per Default `.js`, `.jsx`, `.ts` und `.tsx` Dateien in einem Order / Ordnern namens `__tests__` innerhalb des Projekts
  - Dateien mit Endung `.test.js` oder `.spec.js` innerhalb des Projekts

*Skript in `package.json` zeigen*

*Ausführen eines Tests mit Fehler*

*VSCode Plugin erklären*

*Ausgabe durchgehen*
  - Durchlaufende/fehlgeschlagene Tests (2 Gründe), 
  - Fehlernachricht: Soll, Ist, Stelle, an der Test fehlgeschlagen ist
  - Coverage, wenn konfiguriert
  - Statistik

*Fehler fixen*

*Erneutes Ausführen der Tests*

# Konfiguration

Wie bei vielen Tools über CLI und/oder Konfiguration über package.json und/oder Konfigurationsdatei

*Konfiguration durchgehen*

# Code Coverage

*Hintergründe erklären*
  - per default lcov Coverage Report
  - Wissen, was schon getestet wird und was noch nicht --> Wissen, für was Tests schreiben sind
  - Ansonsten schwer, alle Pfade die Code nehmen kann, zu testen
  - In Jest integriert per Default

*Ausgabe in `coverage` Ordner (HTML) zeigen und Coverage Report durchgehen*

# Optional

## Integration mit Vue

*example_app öffnen und `package.json` zeigen*
  - `@vue/test-utils`
  - `vue-jest`
  - `@vue/cli-plugin-unit-jest`

## Matcher

- Truthiness, z. B. `toBeNull()`, `toBeTruthy()`
- Numbers, z. B. `toBeGreaterThanOrEqual(number)`, `toBe(number)`
- Strings, z. B. `toMatch(string)`
- Iterables, z. B. `toContain(element)`
- Exceptions, z. B. `toThrow()`

## Testen asynchronen Codes

## Mock Funktionen

Ersetze Funktionalität, die du nicht kontrollierst, durch solche die du kontrollierst, um bestimmte Funktionalitäten isoliert testen zu können

Mock Funktionen ermöglichen:
  - Fangen von Aufrufen
  - Setzen von Rückgaben
  - Veränderungen der Implementierung

Mocking einzelner Funktionen mit `jest.fn`
Mocking aller Funktionen eines Moduls mit `jest.mock`
Mocking mit Möglichkeit des Wiederherstellens der ursprünglichen Funktion mit `jest.spy`

# Ausblick auf weitere Features

- Snapshot Testing
- Testen mit Datenbanken
- Mocks von Timern zur Kontrolle der Zeitintervalle
