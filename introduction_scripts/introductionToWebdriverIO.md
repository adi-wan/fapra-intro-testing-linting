# Installation

*package.json zeigen*

Pakete:
  - `@wdio/cli` - einziges Paket, dass selbst installiert werden muss; Kommandozeilenprogramm zur Interaktion und zum Setup von WebdriverIO
  - `@wdio/sync` zum synchronen Ausführen von WebdriverIO Kommandos; überschreibt globale Funktionen (`describe()`, `it()`, ...) des Testframeworks (z. B. Jasmine)
  - `@wdio/jasmine-framework` - Adapter für Jasmine Framework zum Schreiben der Tests
  - `@wdio/local-runner` zum Ausführen der Tests
  - Service zum Steuern des Browsers oder der App (der eigentliche WebDriver)
    - `@wdio/selenium-standalone-service` - Selenium Server steuern
    - `wdio-chromedriver-service` - Service, der den ChromeDriver wrappt, sodass er vom Testrunner von WDIO zur direkten Kontrolle des Chrome-Browsers (ohne Selenium Server) genutzt werden kann; schnell, aber auf Chrome eingeschränkt; startet Chromedriver von selbst
    - `chromedriver` - eigentlicher Driver
  - `@wdio/allure-reporter` - javabasierter Reporter (startet Server)
  - `@wdio/spec-reporter` - Reporter für das Terminal
  - `@types/jasmine` - Typen für VSCode (Autovervollständigung, Dokumentation usw.)

*Set up mittels `./node_modules/.bin/wdio config` zeigen*

# Tests schreiben

*Zeige und erkläre Test*

`waitForExist()`
  - Überprüft in regelmäßige Abständen, ob Element mit der Id 'app' schon erzeugt ist
  - Prüfung läuft bis Intervall terminiert
  - Wenn Intervall terminiert, bevor Element erzeugt ist, wird ein Fehler geworfen
  - der 2. Parameter negiert den Befehl, d. h., statt das gewartet wird, bis Element mit Id 'app' erzeugt ist, wird gewartet, bis dieses Element nicht mehr existiert

# Ausführung

*Command in package.json zeigen*

Es existiert auch ein vue-cli-plugin, aber leider habe ich schlechte Erfahrungen damit gemacht.

*Führe Test aus*
*Gehe Ausgabe durch*
*Ändere Loglevel
*Debugge Test mithilfe von Debug Statement, passe Test an, damit er durchläuft*
*Führe Test erneut aus*
*Allure Report zeigen*

# Konfiguration

*Konfigurationsdatei zeigen: von oben nach unten durchgehen*

# Integration in Gitlab zeigen, falls noch Zeit ist
