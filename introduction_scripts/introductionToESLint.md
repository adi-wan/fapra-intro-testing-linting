# Allgemein

*Kurze Vorstellung der Beispielapp*
  - Im Browser alle Funktionalitäten zeigen
  - Codebasis präsentieren

Einführung von ESLint am Beispiel des Frontends

# Installation

*Pakete in `package.json` zeigen*
  - `eslint`
  - `eslint-plugin-vue`: Ermöglicht Überprüfung von `<template>` und `<script>` von `.vue` Dateien. Findet syntaktische Fehler und fehlerhafte von Vue.js Directives sowie Verletzungen des Vue.js Style Guides.
  - `eslint-plugin-import`: Ermöglicht Linting von ES2015+ (ES6+) import/export Syntax und verhindert Probleme mit der fehlerhaften Angaben von Dateipfaden und Importbezeichnungen.
  - `@vue/cli-plugin-eslint`: Erlaubt die Nutzung von ESLint über Vue CLI und integriert ESLint mit Webpack Server, den Vue in der Entwicklungsumgebung (im Sinn von Phase der Entwicklung, nicht die IDE) nutzt.
  - `babel-eslint`: Wrapper des Babel Parsers kompatibel zu ESLint. Ermöglicht das Linting von Features, welche bereits von Babel, aber noch nicht von ESLint unterstützt werden. Im Prinzip wird ESLint gepatcht und der Code so transformiert, dass ESLint ihn versteht.

## Exkurs: Was ist Babel?

Transpiler, d. h. eine Sammlung von Werkzeugen, der ECMASkript 2015+ in eine abwärtskompatible Version von JavaScript umwandelt, das in aktuellen und älteren Browsern oder Umgebungen unterstützt wird (durch Transformation der Syntax und Polyfill, d. h. neue Konstruktte werden durch ältere ersetzt bzw. simuliert)
- `@babel/core` enthält Kernfunktionalität, andere Werkzeuge stellen Schnittstellen dar und Integrationen mit übriger technischer Infrastruktur
- `@babel/cli` ist Kommandozeilenprogramm
- Transformationen in Form von Plugins (z. B. zur Transformation von Arrow Funktionen)
- Preset ist eine Menge von Plugins
- `@babel/preset-env` ist ein Preset, dass alle Plugins enthält, die zur Transformation modernen ECMASkripts notwendig sind
- CLI und Preset Optionen können über CLI oder Konfiguration (`.babelrc` für statische Konfiguration (vs. `babel.config.js`) übergeben werden

Wie installiert?
  - `npm install eslint --save-dev`
  - tw. werden Pakete durch ESLint installiert, nach dem dieses über das Kommandozeilenprogramm initialisiert wurde
  - tw. werden Pakete durch Aufsetzen von Projekt mit VUE CLI installiert

# Initialisierung

(nicht beenden, da bereits initialisiert)

*Init. mithilfe von `./node_modules/.bin/eslint --init`*

*[Erklärung der Initialisierungsoptionen](https://dev.to/iamdammak/setting-up-eslint-in-your-javascript-project-with-vs-code-2amf)*
  - "How would you like to define a style for your project?" "Inspect your JavaScript files" setzt alle Regeln, welche nicht durch Code verletzt werden auf Default Fehlerniveau "error".

Init. legt automatisch `.eslintrc` im Wurzelverzeichnis des Projekts an. Alternativ kann Konfiguration auch in package.json hinterlegt werden.

# Konfiguration

*`.eslintrc.js` durchgehen und einzelne Optionen erklären*

`globals`
  - hiermit beginnen, da für Verständnis von `env` wertvoll
  - `no-undef` Regel warnt, wenn auf Variablen zugegriffen wird, die nicht innerhalb derselben Datei definiert wurden, außer ESLint bekommt mitgeteilt, dass es sich dabei um globale Variablen handelt. Dies kann unter dieser Option und auch innerhalb einiger anderer Optionen rfolgen oder über Kommentare in der jeweiligen Datei erfolgen.
  - bei den hinterlegten globalen Variablen handelt es sich um solche, die mit ES2017 eingeführt wurden und die automatisch durch ESLint hinzugefügt wurden (hier eigentlich nicht notwendig)

`env`
  - `es6` - enable all ECMAScript 6 features except for modules (this automatically sets the ecmaVersion parser option to 6).
  - `node` - Node.js global variables and Node.js scoping.
  - alle anderen Umgebungen teilen ESLint globale Variablen mit

`extends` the set of enabled rules from base configurations

`parserOptions` 
  - per Default nutzt ESLint Esprima als Parser
  - hier können alternative Parser angegeben werden
  - `sourceType` per Default `script`, altnernativ ECMASkript Module

`plugins`
  - erlaubt uns das Linten von `<script>` und `<template>` Sektionen von `.vue` Dateien

`root`
  - By default, ESLint will look for configuration files in all parent folders up to the root directory. 
  - all of your projects follow a certain convention 
  - to limit ESLint to a specific project place this in the config at your project’s root level

`rules`
  - erhält Objekt mit Regeln als Properties
  - Schlüssel sind die Regelbezeichnungen
  - Werte sind Strings mit Fehlerniveau ("error level", 3 Alternativen: "off", "warn" (kein Einfluss auf Exitcode beim Kompilieren oder Bundlen), "error") oder Arrays, die Fehlerniveau und Optionen zur Regeln enthalten
  - *Regeldokumentation zeigen*
  - Unser Beispiel `semi``
    - "always" (Default) setzt Semikolon nach jeder Anweisung voraus
    - "never" verbietet Semikolons nach Anweisungen mit Ausnahme von Anweisungen bzw. Folgen von Anweisungen, die so Eindeutigkeit verlieren (Anweisungen, die mit [, (, /, +, oder - beginnnen)

# Linten am Beispiel

*`app.js` zeigen, Regel `semi` anschalten und mithilfe der folgenden Möglichkeiten Linten*

*Kommandozeile erklären*
  - `./node_modules/.bin/eslint` öffnet Hilfeseite
  - `./node_modules/.bin/eslint tests/unit/mocking/app.js tests/unit/mocking/math.js`
  - `./node_modules/.bin/eslint .` zum Linten des ganzen Ordners
--fix Option

*Auf weitere Einstellungen in `package.json` hinweisen*
  - `lint` Skript (durch Vue CLI automatisch gesetzt) 

*Hinweis auf VSCode Plugin*
  - Settings können verändert werden, z.B. `autofixOnSave`
  - Kommandos werden hinzugefügt
  - Server
  - *In `app.js`zeigen, Popover öffnen, Möglichkeiten zum Umgang mit Regelverletzungen demonstrieren*
  - Erklären, dass ESLint auch mit Kommentaren gesteuert werden kann
    - `disable-eslint` muss an Anfang von Skript-Sektion, nimmt komma-separierte Liste von Regeln entgegen

*Starten des Dev Servers mit Linting auf "off" vs. "warn" vs. "error"*

# Optional

[Ausblick auf das Definieren eigener Regeln](https://eslint.org/docs/developer-guide/working-with-rules)

Integration in Gitlab Pipeline zeigen
