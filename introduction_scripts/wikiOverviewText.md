## Überblick

### Linter

Ein Linter ist ein Programm, das der statischen Analyse (dem „Linten“) von Programmen im Hinblick auf programmatische und stilistische Fehler dient. Gerade in Skriptsprachen, wie JavaScript (JS), denen ein klassischer Compiler, wie man ihn z. B. von Java kennt und der schon während dem Editieren auf Fehler hinweist, fehlt, können so schon während dem Editieren Fehler gefunden und beseitigt werden. In verteilten Teams lässt sich mithilfe von Lintern außerdem ein einheitlicher Stil bzw. eine einheitliche Codequalität durchsetzen. Aber auch eine einzelne Person kann durch höhere Konsistenz der Codebasis und frühes Erkennen und Berichtigen von Fehlern vom Einsatz von Lintern profitieren.

#### Beispiel

Ein bis heute strittiger Punkt zwischen JavaScript-Entwicklern ist das Setzen eines Semikolons nach jeder Anweisung. Die einen sehen im Weglassen des Semikolons eine Quelle für schwer auffindbare Fehler. Die anderen bemängeln den Schreibaufwand, der im Rahmen von JavaScripts ASI (automatic semicolon insertion) meist eingespart werden kann. Mithilfe eines Linters (im Bsp. wird ESLint integriert mit Visual Studio Code genutzt) lässt sich für ein Projekt ein einheitlicher Stil durchsetzen. Durch die Unterstreichung mit einer roten Wellenlinie an der Stelle, an der ein Semikolon fehlt, wird der Entwickler / die Entwicklerin auf das Fehlen aufmerksam gemacht.

![linter-semi-example-1](uploads/714175eb44f9c0c160f8fc2cc02dd399/linter-semi-example-1.png)

Beim Bewegen der Maus über die Stelle werden nähere Informationen zum Problem, darunter auch der Name der Regel (hier „semi“), präsentiert und eine Reihe von Möglichkeiten zum Umgang mit dem Problem zur Verfügung gestellt (im Beispiel unter „Quick Fix...“).

![linter-semi-example-2](uploads/1fd980f6f4fc2a4663307a2e9a9047b8/linter-semi-example-2.png)
![linter-semi-example-3](uploads/7f02526e232ae69ba35a6eb849239cb0/linter-semi-example-3.png)

#### Vergleich von Lintern in JavaScript

In JavaScript gibt es viele verschiedene Linter. Meist werden die (Stil-) Fehler, welche der Linter betrachten soll, in Regeln festgehalten. Ob und inwieweit EntwicklerInnen diese Regeln ein- und ausschalten oder gar eigene Regeln formulieren und zur Regelbasis hinzufügen können, variiert von Linter zu Linter. Auf der einen Seite des Spektrums sind solche Lösungen, die eine feste Regelbasis enthalten und meist die Ansicht des Autors / der Autorin des Linters darüber widerspiegeln, wie guter Code in JavaScript aussieht. Auf der anderen Seite des Spektrums befinden sich Linter, in denen jede Regel nicht nur ein- und ausschaltbar, sondern auch modifizierbar und die Regelbasis um eigene Regeln erweiterbar ist. Höhere Konfigurier- und Erweiterbarkeit geht in der Regeln auch mit höherer Komplexität in der Einrichtung einher, die jedoch durch Einrichtungsassistenten und importierbare Regelbasen begrenzt werden kann.

Des Weiteren liegt ein wichtiger Unterschied zwischen den aktuellen Lösungen auch darin, dass manche dazu in der Lage sind, zumindest teilweise selbstständig (Stil-) Fehler im Code zu berichtigen, während andere lediglich Hinweise liefern. Ersteres kann dem Entwickler / der Entwicklerin viel Arbeit abnehmen. Nicht zuletzt bleibt bei Wahl eines Linters immer zu betrachten, inwieweit das jeweilige Produkt die genutzte Umgebung (Node.js vs. Browser) bzw. die genutzten Konstrukte (z. B. import/export nach ES16) unterstützt. Auch hier ist die Varianz groß. Eng damit verbunden ist auch die Integration des Linters mit der übrigen technischen Infrastruktur, z. B. Frameworks, wie Vue oder React, oder der Entwicklungsumgebung (z. B. Visual Studio Code).

### Testframeworks

Testen ist heutzutage ein integraler Bestandteil jedes Softwareentwicklungsprojekts, um die Erfüllung der funktionalen wie auch nicht funktionalen Anforderungen (insbesondere natürlich der Korrektheit) sicherzustellen. Testen schließt im weiten Sinne zwar auch das manuelle „Durchklicken“ durch eine Applikation ein, jedoch ist im Folgenden das automatisierte Testen gemeint, wobei Tests in Code verfasst oder mittels spezieller Recorder aufgezeichnet werden. Damit wird das Testen systematisiert und wiederholbar. Genau für diese Art des Testens bietet eine nahezu unüberschaubare Zahl von Testframeworks Lösungen an. 

#### Beispiele

Das folgende Beispiel ist ein Unit-Test. Der Test ist mit dem Jest-Framewok verfasst worden und wird auch mit dem zum Framework gehörigen Testrunner zur Ausführung gebracht. Getestet wird die Funktion `filterByTerm()`, welche ein Array mit Objekten, die jeweils eine Url enthalten, und einen String erhält. Sie liefert ein Array mit genau den Objekten des ursprünglichen Arrays zurück, deren Urls den String enthalten, wobei Groß- und Kleinschreibung nicht beachtet wird.

```javascript
// filterByTerm.js

const filterByTerm = (inputArr, term) => inputArr.filter(arrayEl => arrayEl.url.toLowerCase().match(term.toLowerCase()))

export default filterByTerm
```

```javascript
// filterByTerm.test.js

import filterByTerm from "../src/filterByTerm.js"

describe('Filter function', () => {
  test('should filter by a search term (link)', () => {
    const input = [
      { id: 1, url: 'https://www.url1.dev' },
      { id: 2, url: 'https://www.url2.dev' },
      { id: 3, url: 'https://www.link3.dev' },
    ]
    const output = [{ id: 3, url: 'https://www.link3.dev' }]
    expect(filterByTerm(input, 'link')).toEqual(output)
  })
})
```

Am Beispiel lässt sich der grundsätzliche Aufbau eines Tests nachvollziehen, der sich nur wenig zwischen den meisten der (Unit-) Testframeworks in JS unterscheidet. Zunächst wird das zu Testende, hier die Funktion `filterByTerm()`, importiert. Mit dem Aufruf der Funktion `describe()` wird eine Testsuite eröffnet. Der erste Parameter ist der Name des Testsuite, der zweite eine Funktion, welche entweder weitere Testsuites oder die einzelnen Tests in Form von Aufrufen der `test()` Methode enthält. Diese bekommen als Argumente einen Testnamen und eine Funktion mit dem eigentlichen Test übergeben. Der Inhalt des Tests besteht aus der Deklaration eines Inputs, des erwarteten Outputs und schließlich dem eigentlichen Test, welcher die Funktion mit dem Testinput zur Ausführung bringt und das Ergebnis mit dem erwarteten Output vergleicht. Die Assertion, d. h. die Prüfung, wird hier mit der `expect()` Funktion formuliert. An den Aufruf schließt sich direkt der Aufruf eines sogenannten Matchers, hier `toEqual()` an, der die Art der Prüfung definiert (hier auf Gleichheit).

#### Vergleich von Testframeworks in JavaScript

Grundsätzlich lassen sich die Frameworks in JavaScript danach einteilen, welche Arten von Testtypen sie unterstützen. So ist der Test aus dem Beispiel und das verwendete Framework Jest ein klassisches Beispiel für ein Framework, das (zumindest für sich genommen) auf Unit- und Integrationstests fokussiert, also Tests, die sich entweder nur einer Funktion, einem Modul, einer Klasse usw. oder der Integration dieser miteinander widmen. Andere Testframeworks fokussieren sich ausschließlich auf funktionale bzw. End-to-End-Tests, welche das „klassische Durchklicken“ durch eine Applikation, simulieren. Ansonsten liegen wesentliche Differenzen in den Features bzw. Tools, welche ein Testframework mit sich bringt. Mocha beispielsweise ist ein Framework, das im Wesentlichen nur die grundsätzliche Teststruktur, den Testrunner, also das den Test ausführende Programm, und Reporter mitbringt. Dem Entwickler / der Entwicklerin bleibt u. a. überlassen, aus welchen Frameworks Assertions oder Mocks, also andere Programmeinheiten simulierende Programmeinheiten, genutzt werden. Damit verbunden ist die Konsequenz, dass Mocha nicht out-of-the-box funktioniert, sondern noch mithilfe anderer Frameworks erweitert und konfiguriert werden muss. Demgegenüber stehen Frameworks, wie Jest oder Jasmine, welche schon alle notwendigen Tools für typische Tests mitbringen und nur für ganz bestimmte Aufgabenstellungen erweitert werden müssen. Spätestens bei funktionalen Tests spielt es auch eine entscheidende Rolle, in welcher Umgebung (Node.js vs. Browser) die Tests laufen können.
