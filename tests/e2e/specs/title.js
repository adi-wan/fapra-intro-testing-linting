describe('The FaPra CSCW example app for this introduction', () => {
  it('should have the title "FaPra CSCW Example App"', () => {
    const title = browser.getTitle()
    expect(title).toBe('FaPra CSCW Example App')
  })
})
