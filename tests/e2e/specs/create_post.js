describe('post', () => {
  const WAIT_FOR = 3000

  it('with current date and text entered should be created by hitting "Post!" button', () => {
    // Wait for app to be mounted
    $('#app').waitForExist(WAIT_FOR, false, 'App was not mounted.')

    // Formulate expectations
    const expectedPostIndex = $$('.post').length
    const currentDate = new Date()
    const expectedPostDate = `${currentDate.getDate()}/${currentDate.getMonth()}/${currentDate.getFullYear()}`
    const expectedPostText = 'WDIO is great!'
    const expectedPostTextInKeys = expectedPostText.split('')

    // Interact with application
    $('#create-post-input').click()
    browser.keys(expectedPostTextInKeys)
    $('#create-post-button').click()

    // Check if expectations are met (need to wait because backend needs time to answer)
    browser.waitUntil(() => $$('.post')[expectedPostIndex], WAIT_FOR, 'Post was not not created.')
    expect($$('.post-date')[expectedPostIndex].getText()).toBe(expectedPostDate)
    expect($$('.post-text')[expectedPostIndex].getText()).toBe(expectedPostText)

    // Clean up (optional)
    $$('.post')[expectedPostIndex].doubleClick()
  })
})
