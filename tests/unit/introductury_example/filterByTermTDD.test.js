// 2. implementation
import filterByTerm from './filterByTerm'

// 1. write (positive) test(s)
describe('Filter function', () => {
  // 3. refactoring
  let input
  beforeEach(() => {
    input = [
      { id: 1, url: 'https://www.url1.dev' },
      { id: 2, url: 'https://www.url2.dev' },
      { id: 3, url: 'https://www.link3.dev' }
    ]
  })

  // 1.
  test('should filter by a search term (link) (result not empty)', () => {
    const output = [{ id: 3, url: 'https://www.link3.dev' }]

    expect(filterByTerm(input, 'link')).toEqual(output)
    expect(filterByTerm(input, 'LINK')).toEqual(output)
    expect(filterByTerm(input, 'lINK')).toEqual(output)
  })

  // 3. negative and further test(s)
  test('should filter by a search term (dieLinke) (result empty)', () => {
    const output = []

    expect(filterByTerm(input, 'dieLinke')).toEqual(output)
  })

  test('should return input if filtering with empty search term)', () => {
    const output = input

    expect(filterByTerm(input, '')).toEqual(output)
  })
})
