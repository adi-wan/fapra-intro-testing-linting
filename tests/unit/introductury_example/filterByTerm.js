/**
 * Receives an array of objects that each have an url property whose value is an url and a term.
 * Returns an array of the objects of the input array whose url contains the term (irrespective of case).
 *
 * @param {Array} inputArr Array of objects with an url property whose value is an url in form of a string.
 * @param {String} term Term that the urls are to be matched against.
 */
const filterByTerm = (inputArr, term) => inputArr.filter(arrayEl => arrayEl.url.toLowerCase().match(term.toLowerCase()))

export default filterByTerm
