const sum = (...args) => args.reduce((result, arg) => result + arg, 0)

const dotProduct = (v, w) => sum(...v.map((valueV, index) => valueV * w[index]))

export default {
  dotProduct,
  sum
}
