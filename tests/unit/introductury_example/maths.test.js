import maths from './maths'

describe('maths:', () => {
  describe('sum', () => {
    test('of 1 and 2 should be 3', () => {
      expect(maths.sum(1, 2)).toBe(3)
    })
    test('of 5, 12, -27 should be -10', () => {
      expect(maths.sum(5, 12, -27)).toBe(-10)
    })
  })
  describe('dot product', () => {
    test(`of (${[1, 2, -3]}) and (${[5, 6, 9]}) should be -10`, () => {
      expect(maths.dotProduct([1, 2, -3], [5, 6, 9])).toBe(-10)
    })
  })
})
