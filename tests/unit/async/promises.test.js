test('the data is peanut butter', () => {
  const fetchData = jest.fn().mockResolvedValue('peanut butter')
  return fetchData().then(data => {
    expect(data).toBe('peanut butter')
  })
})

test('the fetch fails with an error', () => {
  const fetchData = jest.fn().mockRejectedValue('error')
  expect.assertions(1)
  return fetchData().catch(e => expect(e).toMatch('error'))
})

test('the data is peanut butter', async () => {
  const fetchData = jest.fn().mockResolvedValue('peanut butter')
  const data = await fetchData()
  expect(data).toBe('peanut butter')
})

test('the fetch fails with an error', async () => {
  const fetchData = jest.fn().mockRejectedValue('error')
  expect.assertions(1)
  try {
    await fetchData()
  } catch (e) {
    expect(e).toMatch('error')
  }
})
